logger         = require('log4js').getLogger 'CAPTCHA'
Hookable       = require './hookable'
module.exports = class Captcha extends Hookable
    locators = {}

    save = (done) ->
      @app.browser.screenshot locators.challenge.apply(@app), (err, image) ->
        return done err if err
        done null, image

    enter_solution = (solution, done) ->
      @app.browser.enter_value locators.solution.apply(@app), solution, done

    constructor: (@captcha, @app) ->
      locators.challenge   = @to_function @captcha.locators.challenge
      locators.solution    = @to_function @captcha.locators.solution
      super
        pre_challenge: @to_function @captcha.hooks.pre_challenge
        post_challenge: @to_function @captcha.hooks.post_challenge

    solve: (done) ->
      _t = @
      cb = (solution) ->
        logger.debug "Got solution '#{solution}'"
        enter_solution.apply _t, [solution , ->
          _t.hook 'post_challenge', ->
            done()
        ]

      _t.hook 'pre_challenge', ->
        save.apply _t, [ (err, image) ->
          return done err if err
          logger.debug 'Got captcha image buffer'
          if _t.app.profile.manual_anticaptcha
            _t.app.manual_anticaptcha.process image, cb
          else
            _t.app.anticaptcha.process image, cb
        ]
