logger         = require('log4js').getLogger 'UTILS'
cs             = require 'coffee-script'
module.exports = {
    read_stream: (stream, encoding, done) ->
      if typeof encoding == 'function'
        done = encoding
        encoding = 'utf-8'
      data = ''
      stream.on 'readable', -> data += stream.read().toString(encoding)
      stream.on 'end', -> done data

    http_error_handler: (err) -> logger.error "#{err}"

    coffee_compile: (coffee) -> cs.compile coffee, bare: true
  }
