logger         = require('log4js').getLogger 'ANTICAPTCHA'
querystring    = require 'querystring'
request        = require 'request'
module.exports = class AntiCaptcha
    base_url = 'http://2captcha.com'

    _request = (path, method = 'GET', done) ->
      if typeof path != 'string'
        body =  path[1]
        path = path[0]

      logger.debug "Requesing #{path}"
      request
        url: "#{base_url}#{path}"
        body: if method == 'POST' then querystring.stringify body else null
        method: method
        qs: if method == 'GET' then body else null
        headers:
          'Content-Type': 'application/x-www-form-urlencoded',
        , (err, res, body) ->
          throw err if err
          if res.statusCode != 200
            throw new Error "Wrong response status: #{res.statusCode}"
          done body

    send_challenge = (challenge, done) ->
      throw new Error "Challenge image is empty." if !challenge
      _t = @
      _request [
          '/in.php'
          method: 'base64'
          body: challenge.toString 'base64'
          key: @key
        ], 'POST', (challenge_id) ->
          switch challenge_id
            when 'ERROR_NO_SLOT_AVAILABLE'
              logger.warn "No slots available. Try to increase rate. Waiting..."
              setTimeout (-> send_challenge.apply _t, [challenge, done]), 15000
            when 'ERROR_KEY_DOES_NOT_EXIST'
              throw new Error 'Wrong AntiCaptcha API key.'
            when 'ERROR_ZERO_BALANCE'
              throw new Error 'Refill your AntiCaptcha account for a certain amount.'
            when 'ERROR_WRONG_USER_KEY', 'ERROR_ZERO_CAPTCHA_FILESIZE', \
            'ERROR_TOO_BIG_CAPTCHA_FILESIZE', 'ERROR_WRONG_FILE_EXTENSION', \
            'ERROR_IMAGE_TYPE_NOT_SUPPORTED', 'ERROR_IP_NOT_ALLOWED', \
            'IP_BANNED'
              throw new Error "Can't upload #{challenge} to AntiCaptcha service - #{response}."
            else
              _t.last_challenge_id = challenge_id.replace('OK|', '').trim()
              done _t.last_challenge_id

    get_status = (challenge_id, done) ->
      _t = @
      _request [
          '/res.php'
          key: @key
          action: 'get'
          id: challenge_id
        ], 'GET', (response) ->
          switch response
            when 'CAPCHA_NOT_READY'
              logger.debug 'Captcha not ready. Waiting...'
              setTimeout (-> get_status.apply _t, [challenge_id, done]), 15000
            when 'ERROR_KEY_DOES_NOT_EXIST', 'ERROR_WRONG_ID_FORMAT', \
            'ERROR_WRONG_CAPTCHA_ID', 'ERROR_NO_SUCH_CAPCHA_ID', \
            'ERROR_CAPTCHA_UNSOLVABLE'
              throw new Error "Can't get status ID #{challenge_id} - #{response}."
            else done response.replace 'OK|', ''

    report_bad: (done) ->
      _t = @
      _request [
          '/res.php'
          key: @key
          action: 'reportbad'
          id: @last_challenge_id
        ], 'GET', (response) ->
          if !response || response != "OK_REPORT_RECORDED"
            throw new Error "Wrong response. Can't report bad captcha."
          logger.warn "Wrong captcha with ID# #{_t.last_challenge_id} reported."
          done()

    constructor: (@key) ->

    process: (image, done) ->
      _t = @
      logger.debug 'Sending image to service'
      send_challenge.apply _t, [image, (challenge_id) ->
        logger.debug "Uploaded captcha with ID# #{challenge_id}"
        get_status.apply _t, [challenge_id, (solution) ->
          logger.debug "Got solution for ID# #{challenge_id} = '#{solution}'"
          done solution
        ]
      ]
