logger         = require('log4js').getLogger 'DUBCAPTCHA'
querystring    = require 'querystring'
request        = require 'request'
module.exports = class ManualAntiCaptcha
    constructor: (@api) ->

    get_solution: (challenge_id, done) ->
      _t = @
      @api.get_captcha_solution challenge_id, (result) ->
        if !result.success
          done new Error("Can not get captcha #{captcha_id} solution")
        if result.solution == ''
          logger.debug "Captcha not ready, waiting..."
          setTimeout ->
              _t.get_solution challenge_id, done
            , 15000
        else done result.solution

    process: (image, done) ->
      _t = @
      logger.debug 'Sending image to service'
      @api.send_captcha image.toString('base64'), (result) ->
        done new Error('Can not upload captcha') if !result.success
        challenge_id = result.captcha._id
        logger.debug "Uploaded captcha with ID# #{challenge_id}"
        _t.get_solution challenge_id, (solution) ->
          done new Error("Can not get solution for ID# #{challenge_id}") if !solution
          logger.debug "Got solution for ID# #{challenge_id} = '#{solution}'"
          done solution
