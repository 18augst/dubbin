cs             = require 'coffee-script'
module.exports = class Hookable
  coffee_compile = (coffee) -> cs.compile coffee, bare: true

  constructor: (@hooks) ->

  to_function: (code) -> eval coffee_compile code

  hook: (name, done) ->
    return done() if !@hooks[name]
    @hooks[name].apply @, [
      (err) ->
        return done err if err
        done null
    ]
