logger         = require('log4js').getLogger 'FAUCET'
_              = require 'underscore'
Hookable       = require './hookable'
Captcha        = require './captcha'
module.exports = class Faucet extends Hookable


    visit = (done) ->
      @app.browser.go @faucet.ref_url, -> done()

    with_checked_login = (done) ->
      return done() if !@faucet.login
      @app.browser.is_present_and_visible @locators.login_proof.apply(@app), (err, el) ->
        if err
          return done(new Error """
                          You're not signed in on this faucet.
                          To sign in:
                          1. Run FireFox with 'dubbin' profile
                          2. Manually visit faucet
                          3. Sign in with you credentials
                          4. Quit FireFox and start 'dubbin'
                          """)
        done()

    with_entered_address = (done) ->
      return done() if !@faucet.locators.wallet_address
      _t = @
      @app.browser.enter_value @locators.wallet_address.apply(@app),
        _t.options.wallet_address, (err) ->
          return done err if err
          done()

    claim = (done) ->
      _t = @
      with_entered_address.apply _t, [ (err) ->
        return done err if err
        _t.options.captcha.solve (err) ->
          return done err if err
          _t.app.browser.click _t.locators.claim_button.apply(_t.app), ->
            logger.debug "Claim button pressed"
            done()
      ]

    get_result = (done) ->
      _t = @
      @app.browser.is_present_and_visible @locators.success_proof.apply(@app), (err, el) ->
        if err
          logger.warn "Success proof is not present. (The most probably failed to get reward)."
          if _t.app.profile.manual_anticaptcha
            done()
          else
            logger.debug "Checking if captcha solution wasn't correct."
            _t.app.browser.is_present_and_visible _t.locators.wrong_captcha.apply(_t.app), (err, el) ->
              if !err
                _t.app.anticaptcha.report_bad ->
                  _t.app.api.log {type: 'wrong_captcha'}, _t.faucet._id, (r) ->
                    done()
        else
          el.getText().then (text) ->
            try
              logger.debug "#{text}"
              amount = text.match(/(\d+)/ig)
              logger.debug amount
              amount = parseInt(amount.pop()) if amount
              logger.debug "#{amount}"
              amount = if isNaN amount then 0 else amount
              _t.app.api.log {type: 'claim', amount: amount}, _t.faucet._id, (r) ->
                logger.info " >>> #$*@%^! Got #{amount} satoshis! YAY!!! <<<"
                done()
            catch e
              logger.error e

    constructor: (@faucet, @app) ->
      logger.debug "Constucting #{@faucet.name}"
      @locators = {}
      @options  = {}
      # Options
      @options.captcha         = new Captcha @faucet.captcha, @app
      @options.wallet_address  = @app.profile[@faucet.currency.toLowerCase() + '_address']
      logger.debug "Claiming to #{@options.wallet_address}"
      # Locators
      @locators.wallet_address = @to_function @faucet.locators.wallet_address
      @locators.claim_button   = @to_function @faucet.locators.claim_button
      @locators.success_proof  = @to_function @faucet.locators.success_proof
      @locators.wrong_captcha  = @to_function @faucet.locators.wrong_captcha
      @locators.balance        = @to_function @faucet.locators.balance
      @locators.login_proof    = @to_function @faucet.locators.login_proof
      # Hooks
      super
        pre_claim: @to_function @faucet.hooks.pre_claim
        # post_claim: @to_function @faucet.hooks.post_claim
      # Public
      @wallet_address         = @options.wallet_address
      console.log @wallet_address
      @browser                = @app.browser
      @By                     = @browser.By
      # Last claim check
      client_offset = (new Date).getTimezoneOffset() * 60 * 1000
      if @faucet.events.length > 0
        @last_visited = new Date(_.first(@faucet.events).created).getTime()# - client_offset
      else @last_visited = Date.now() - 24 * 60 * 60 * 1000
      #                                  H    M    S     ms

    dub: (done) ->
      _t = @

      if @options.wallet_address == undefined || @wallet_address == undefined
        logger.warn """
                    Wallet address is not defined for #{@faucet.currency}.
                    Skipping faucet.
                    """
        return done()

      logger.debug "Visiting #{@faucet.name}"
      visit.apply _t, [ ->
        logger.debug "Checking if you're logged in"
        with_checked_login.apply _t, [ (err) ->
          return done err if err
          logger.debug "Calling pre claim hook"
          _t.hook 'pre_claim', (err) ->
            return done err if err
            logger.debug "Getting reward"
            claim.apply _t, [ (err) ->
              return done err if err
              logger.debug "How much #{_t.faucet.currency} you got? Got you?!"
              get_result.apply _t, [ ->
                _t.last_visited = Date.now()
                logger.debug "Finished with #{_t.faucet.name}"
                done()
              ]
            ]
        ]
      ]
