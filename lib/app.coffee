logger         = require('log4js').getLogger 'DUB.BIN'
# Helpers
_              = require 'underscore'

# App modules
Api               = require './api'
Browser           = require './browser'
AntiCaptcha       = require './anticaptcha'
ManualAntiCaptcha = require './manual_anticaptcha'
Faucet            = require './faucet'

module.exports = class App
    with_profile = (app, done) ->
      app.api.profile (profile) -> done profile

    with_faucets = (app, done) ->
      result = []
      app.api.faucets (faucets) ->
        result.push(new Faucet faucet, app) for faucet in faucets
        done result

    constructor: (done) ->
      _t = @
      logger.debug 'Init()'
      @config = require '../config.json'
      new Browser @config.profile, (browser) ->
        _t.browser            = browser
        _t.By                 = browser.By
        _t.api                = new Api _t.config.email, _t.config.api_key
        _t.manual_anticaptcha = new ManualAntiCaptcha _t.api

        with_profile _t, (profile) ->
          _t.profile     = profile
          _t.anticaptcha = new AntiCaptcha profile.anticaptcha_key
          with_faucets _t, (faucets) ->
            _t.faucets = faucets
            done _t

    rotate: (done) ->
      _t = @
      now       = Date.now()
      available = _.filter @faucets, (f) ->
        # console.log "Now is #{new Date(now)} and visited at #{new Date(f.last_visited)}"
        now - f.last_visited > f.faucet.interval * 60000

      if available.length > 0
        sorted_by_profit = _.sortBy available, (faucet) ->
          parseFloat faucet.average_claim
        done _.first sorted_by_profit
      else
        logger.info 'No faucets available to claim now, waiting...'
        setTimeout ->
            _t.rotate done
          , 5000

    start: (callback) ->
      _t = @
      @browser.maximize()
      @rotate (f) ->
        logger.debug "Last visited #{f.faucet.name} at #{new Date f.last_visited}"
        f.dub (err) ->
          if err
            logger.error "Failed to get reward from #{f.faucet.name}. #{err.message}. Please, report a problem."
            f.last_visited = new Date().getTime()
          logger.info 'Going to next faucet...'
          _t.start callback

    test: (faucet_name, callback) ->
      logger.info "Test #{faucet_name} started"
      _t = @
      @profile.manual_anticaptcha = true
      @api.faucet faucet_name, (faucet_data) ->
        f = new Faucet faucet_data, _t
        f.dub (err) ->
          logger.info 'Test finished'
          if err
            _t.api.log
                type: 'unhandled_error'
                message: err.message
              , faucet_data._id, (r) ->
                logger.error err.message
                callback err
          else
            callback()
