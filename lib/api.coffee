logger         = require('log4js').getLogger 'DUB.API'
request        = require 'request'
module.exports = class API
    api_base = "https://bit.dub.co.in/api"
    _profile = null
    _faucets = null

    _request = (path, email, key, done) ->
      body = {}
      method = 'GET'
      if typeof path != 'string'
        method = path[2]
        body = path[1]
        path = path[0]

      request
        rejectUnauthorized: false
        url: "#{api_base}#{path}"
        json: true
        body: body
        method: method
        headers:
          'dub-api-email': email
          'dub-api-key': key
          'User-Agent': 'DUB.BIN'
        , (err, res, body) ->
          if err
            logger.error "#{err}"
            return done err, null
          if res.statusCode != 200
            logger.debug body
            logger.error "Wrong response status: #{res.statusCode}"
            return done true, null
          logger.debug "#{path} requested with #{res.statusCode} and answer: #{body}"
          done null, body

    constructor: (@email, @key) ->
      logger.info "Initialized with '#{@email}' '#{@key}'"

    profile: (done) ->
      if !_profile
        _request '/profile', @email, @key, (err, data) ->
          done(_profile = if err then err else data)
      else done _profile

    faucets: (done) ->
      if !_faucets
        _request "/faucets", @email, @key, (err, data) ->
          done(_faucets = if err then err else data)
      else done(_faucets)

    faucet: (name, done) ->
      _request ["/faucet", { name: name }, "GET"], @email, @key, (err, data) ->
        return done err if err
        done result = if data == null then false else data

    log: (_event, faucet_id, done) ->
      _request ["/log", {event: _event, faucet_id: faucet_id}, "GET"], @email, @key, (err, data) ->
        done(result = if err then err else data)

    send_captcha: (base64_png, done) ->
      _request ["/captcha/upload", base64_png: base64_png, "POST"], @email, @key, (err, data) ->
        done(result = if err then err else data)

    get_captcha_solution: (captcha_id, done) ->
      _request "/captcha/#{captcha_id}/solution", @email, @key, (err, data) ->
        done(result = if err then err else data)
