logger         = require('log4js').getLogger 'BROWSER'
PNG            = require './png'
webdriver      = require 'selenium-webdriver'
FirefoxProfile = require 'firefox-profile'

module.exports = class Browser
    driver          = null
    captchas_path   = '/home/augst/tmp/captchas'


    constructor: (ff_profile_path, done) ->
      _t = @
      @By = webdriver.By

      firefox_profile = new FirefoxProfile
        profileDirectory: ff_profile_path

      firefox_profile.encoded (encoded_fp) ->
        driver = new webdriver.Builder()
          .usingServer 'http://localhost:4444/wd/hub'
          .withCapabilities
            browserName: 'firefox'
            firefox_profile: encoded_fp
          .build()
        driver.getWindowHandle().then (handle) ->
          logger.debug "Instance created #{handle}"
          done _t

    go: (address, done) ->
      logger.debug "Navigating to '#{address}'"
      driver.get address
        .then done

    is_present: (locator, done) ->
      driver.isElementPresent locator
        .then (r) ->
          return done new Error "Element is not present #{locator}" if !r
          done()

    is_visible: (el, done) ->
      el.isDisplayed locator
        .then (r) ->
          return done new Error "Element is not visible #{locator}" if !r
          done()

    is_present_and_visible: (locator, done) ->
      _t = @
      @is_present locator, (err) ->
        return done err, null if err
        _t.element locator, (el) ->
          return done err, null if !el.isDisplayed()
          done null, el

    element: (locator, done) ->
      driver.findElement locator
        .then done

    enter_value: (locator, value, done) ->
      _t = @
      logger.debug "Entering '#{value}' into input #{locator}"
      @is_present_and_visible locator, (err, el) ->
        return done err if err
        el.clear().then ->
          el.sendKeys(value).then done
          # done()

    click: (locator, done) ->
      _t = @
      logger.debug "Clicking element #{locator}"
      @is_present_and_visible locator, (err, el) ->
        return done err if err
        el.click().then done
        # done()

    space_identity: (locator, done) ->
      _t = @
      logger.debug "Getting image dimensions"
      @is_present_and_visible locator, (err) ->
        return done err if err
        _t.element locator, (el) ->
          el.getSize().then (size) ->
            el.getLocation().then (location) ->
              done null, location: location, size: size

    maximize: -> driver.manage().window().maximize()

    scroll_top: (done) ->
      driver.executeScript "window.scroll(0, 0);"
        .then done

    screenshot: (locator, done) ->
      _t = @
      @scroll_top ->
        _t.space_identity locator, (err, si) ->
          return done err if err
          driver.takeScreenshot().then (base64png) ->
            new PNG base64png, si, (image) ->
              done null, image

    close: -> driver.quit()
