logger         = require('log4js').getLogger 'PNG'
sharp          = require 'sharp'
module.exports = class PNG
  guid = ->
    s4 = -> Math.floor (1 + Math.random()) * 0x10000
    s4() + s4()

  constructor: (@base64, dim, done) ->
    sharp new Buffer @base64, 'base64'
      .extract dim.location.y, dim.location.x, dim.size.width, dim.size.height
        .toBuffer (err, buffer, info) ->
          logger.error "#{err}" if err
          done buffer
