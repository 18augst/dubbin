Api = require '../lib/api'

base64_png = "iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAACXBIWXMAAAsSAAALEgHS3X78AAAgAElEQVR4nO3dd3wU1drA8d9szyabToBQpChKFSmCgtJEQcQKiL2g+GK7drnX3r2iYvfarijXcimCqFQBQa8iICIgSIAgNQlJltTN9nn/ACHZ3Ww2m7YDz/fzyR85u3vO2d3ZZ8555syMISUlRUWIWspZsrOpuyCOYaWOMnbt3837X3zItLkf43S7ANA1cb+EECKIzZpAtxO78MoDU1j7+Y+0yzwBOFYClj6T65YdYPPzp2Gp56oVaxdu+vcqdtvt2O1b+GevCFpQUrl4/gGy3zoTa6j/61tD1y9EEzqxbUc+eX4aFpO5csCyMfSTHOx2e/Dfrw/T1Rxh7Uo6YxcdYNtr/Rrox9nA9VdtjLThT/PUiFyeGHQKHdufzqO/ORu81VpTXez+9nNmrzqAr6n7IkQD6N3lNP5v3M0Ygh7Jfp0rJs4hz3+0yO/IYbu7EXt3mN5kRHV78Nf81AZiJOXE1uj3zeT7rQc42ASfQWTKWffynaxr6m4I0YCuvuDKEFPC8j1s3rCe9euP/m3IysOlcmTqteWNa5n0xlI2783Hnp/Fty9dQlsTgI2h/9nIO30NpF29gL12O3b7Oh7qYgZDBoPu/pDvtuZht9vJz1rB+7f3J1V/uN3Ddf/x1k3c/f5KtubYyVt8Ha2qhNQw9QOYT+DiZ7/m9zw79sLK/Tqspj5UkcQFX+Xy80MnYuj4d1bl2rH//iw9LaDEnciY577k13127PZCdv70CZPPaYGxFh9+TXXYhk5j155POS/p0P+mkx9gjd3On++dTcKhN8upT20kb+n1tDIETAlr/J4O98F2Kje9s5Jsux17zka+fPJyrpuTw55pgw63IUTsOCGzbTQ5LAPNx99Dt5WTOL1tc9oMegnH+Ld4/bKW6Cll2dXduWWNl8L/jKR1aiqpqb14ZouB3o98zec36Zh957n0ObUP596/nLaTZzPtqraVhnkGMsbfS/cVt3J663Rajf6UHG/ltqupf7MLMNBi/HNc5ZnGhCFncu6kr0i64e3D/QKU+Aj78Jdivh7dgv7P7cC7/Rn6tUgltes/WO9OYfAL3/Du1Qqf3zSI3n2Gc/+KttwzYy73d48wg6aruY7yTfPZZOjNiJPjAD3pfYdzguokrs9wOpoBQ0vOHJzB3m9/piDkPDDc9wTo0jhv6he8MGQXUy7uR48zr+dz2x08Majy3F8h7dIF2O3fcWPrkFFdiEZjsyaECFjdX+C3A1VzWHsD9rjeH57gsVlZlPn9lG/+nLf+B31GdSaumoaU1KFMnpDC3Em38/qi38jek82vXz7H397Iod/No2hdOVqseZqHPt1Esc+Pq9SBt5o6Q/r9n0x6charNv/B2pnP8vRK5Ui/atWHMHTpw7n38mR+fPAWXlywkZ3Z65j16E1M2dGJm2/vS3w91eE/uJZFf6YwcHBbTNjoMaoDO9+ZyprkQZyZaUBJ7sWIE4v58btduKppJ9z3pGt2DndcGMe3993NOyu3sXfnL3z28L18kQ/KkRpUvPY/+OXXzeS7ZfWLaHoR5LBU3PbtOI48wUdh1m7KjjzuoaSgAuMJNkyEZm5zBl2sabScu4vLAx/MbUeqAf70HKo7f1MWRVFljn0cWL+Jwr9eq1ZQkFOOse2hfnkj6UME0dHUuhcd9Lv4YG3h0QS3ew8/rLIzuWdXmhm+p7yG/kdUh2c/36/I56FzTyf9ra2M6FHBD6/N448zbmN0r2Q+KTifHv71vPtHBYQ8Nhr+e/K26kUH/Z+8tan4aI6wPIvlWT7GVaql+Lu7Gf5dzZ+LEI0hOGAdzmHtqfbHq6L6Q6TBFaXyrrkqnQ6FbF4YMIDnt1QzHtAfqtvn8RLdvlzF7/VVea2qVupXJH2oXXMNXIeT7YvX4Lx6FH27ZXKGeT0Pb/6TnOUF3DeqD6fu74f1j5dYX1JdJVF8T0LEuAZYh6Xi8yvoDUdzHu7dq/jD3YHR57atdhRWl/ojUV99cO/9hR2+ExjUO5UjPTC1YWD/VEo2/05+BKO0SOs4lMfqy7XXXEjz7fPZWOpi57I1qP2u5ZrzwuWvIujDvnVk+9pxVrekoxtBfCeGdJJclYhdwQErvg1devSkZ8+jf6d2bUtCpKFNrSAnu5jkPufQt10z0tOSMRct4Zm3t9P50c957ZahdGvXlhN7DGD0pOd47a4etVvsGaL+uAh+Y/7C+umDv+Bbps4oYuAL/+Kec7vSrkMvxjzxPvd3zOK9N9ZQXo91+A+uZdHOFIZc0Znchasp9INjywI2p49gTMeSsPmrGvuQv5TX51VwzotTmTiwI5ntejH+qRe5tFnVgV/SoBdZsOh1RjU7NtYYC20L3go73MFn3y5j2bKjf8uXPEPfiFdpOlj38hPM8tzAnHVbydq2jHs6efnl6ZGc//Aq2t02nZXr1rN68XSevKQFuVkFtVzsGKL+kyNY1aqW108f/AdZfv8obvlUx1Uffs+6td8yZcgepo67hCkbI1xUGmkdnv18vyIXyGPFyr14ALV4PQu2AK5fWfhHRaS9DtGHAhbdfSkPrmjP5Hlr2LTqY64sf4unfnThdvy19k3BkNaVfn2709ws80jR9CrlsEpZdlVLUsM+fT8fDc3goyplDn68tQMZlUpcO6YzccB0JlZ5notVb9/GiLdvC121L1TdoYWsP4J+4SsM34cgbrKm9CVjStVStWI7MydfyMzJ1bxMtTP3/AzmVvd/JHUA4OTXyV1Irfwc727eG5zKe+HaC/lZBn8eaulvvHfzQN67+XCBuQePrzaQOz8Xz6FnUPjFSFK/qNork7HuE3shohHhwXxxLDKffAU3dN/Fdz9v56ChHcNuf4VbM9Yw+Zs9hwNWaL0692y0PgpRmQSs45hibMmwyc/xTIdEFHwU/j6fF8bfx8e7woUrGDngvEbqoRBVScA6jjk3vczYPi/X6jVdO3bm2guubKAeCRGeHPoREevasTOfPjsNi6m+L+IjRGRkhCXCSoiLp02L1ky8dALjR4xt6u6I45xBLnUrhNAKmRIKITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzGn0dlsv9Jlfvn0l+pTJr/FRmNzut2kssNwj/D9y1+2HWVy4z/Y3/tryE5nJhAiFikoywhBCaIQFLCKEZErCEEJohAUsIoRkSsIQQmiEBSwihGRKwhBCaIQFLCKEZErCEEJohAUsIoRkxfYlkj28PW91byfYcoMhfgY84bPoWtDV1pZu5ORHf27XR+XF4d7DV/Sd7vPkU+ytwY8CsJJJubEMHU2c6GuKp/5vCOynwbGe7ezc5Pjslfhce9JgUMxYlnkR9c1oaM2llyCRdVx+tN3Z7xwoVj38vWa4d7PbkUegvx4UOk2Ij1ZBJa9PJnGJMJYLbAx93YjBgudhfMZfpRTNZ6Cqoctv0KpQ29LeN57qkEXTWx8KPQcXtXc/Ckjl8U/49W33V9hwAnb4rw2xjGJ84iI66ugx0vRS5ljG7ZB6LHJs4EL7ZIyyGTpxq6ceA+GEMtrQjMeLzJxu7vdpxul7mypx52CsXGq7hg1YT6FinNj3sKprAdUW7q5QmJkzls/TTiI+gBlXN49eyGcwsXcxP7tLwT1Za0ythFJclXsQZRmstp0Judhy8lgnFuZXKrAxpMZvHLFGcsev/ift2/521lcuMt/CfzCto3cjn3cZUwFL925lb+BCvludF8OQ9rCqZwqrSL7go/UlujW/VZHsk1b+L5UVTmFqyiRo2wyP8vt9ZUvQ7S4o7MirtYW5PaF/rk7/9/izmFT7Jq+V7qw/s1XB6s/i5LIufy6YzK+VjPkhqizHG2ouGxXQpFxrnMa3yrRW9XzPXdSX3RvNj/Yu6nXmluwMKmzMysWsEwcpDnmMa/yz8hHW+SNvby7rSd1hXOo1Otnv5R8pw2unkrPyYyWH5/b/z79xbIgtWlak7+DJ/Ag8Ub8XRMF0Ly+39lhdyruPJWgSrKtQdfFNwI5PsP2GvRRTw+9fxr5xbeCWK4BHUBbXmGhq7vagpbRiZ2IWqP+2DLC1ZT0nUlaqUO2ezODDYmC7hAqOphteWsKHobq47UItgVYWLrNJnuSHnFX7weaOp4JgSIyOsAywteI3p7qrfqMlwCqea29FSFwdqMQc8f/Cbaz8VQa938tvBu3lC9z5P2zIbZM8disfzNY/nvMiP/mqeoGvBSaZ2ZOoTsSg+HL79ZLu2sC/o+Sp/lvydu3iVt1JPJaHGlgtYWvAwMzyhf/gGfTtOMbWlpT6ZeJ0eVXVQ7i+jyLuHbPfuWgXGpmmvLvQ0s46nv/1RfqrUrqNiBqt8/ThXH80+uojVJSsDdkg6eiQOo1XYQY+DP4ru4+6iLELHKgPppu50MqZjU3yUe/exw7WVnBCfl+r5kodzVJ5veRf9o3oPx4aYCFiOijeZ6i8/8r/VfBl3pl7DOebkoA76/TtZWfwGrxT/QlHVWvi58HFmWt7gyhr3enXn9//CW3mhgpVCpvVqbki6iEHmdIJ74mR/xVdMs7/DYk/VPebukkd4yfIRj1hTwg59na7/8C9H4HhSoU38RG5NHk0/Y0KY1/txeDfzs2MlK8oX8Z2rOOz7bIr26kqn78P4+GR+Kqu0hai/MrM8l6GJmbXe6H3e75lR4a5aqJzB5db0sO+7yPEqfw8ZrJLplXQHk5IGcZIusDfl7HLM4h37h/wYOKDyzuOx/JP4sPloMo/T2WFshGp/yZFpRmvbs0xvcQcjQgQrAJ2uPYNTpvBRiys5KejRLD4oWBByD1W/Slhjf4o5QSP0dlyc8RkfZUxgeMhgBWAhM24s/8j8mIfi04PqXV74Luv94d6Ak62lyygMKG2f/Cb/anYFZ4QNHgA6rIZuDEm8lcdbzubLzCe41pIY5ohlY7dXH6x0TryAzIDSbSXz2VXrbcPD3rJZbAkoTY4fx2lhckqqfx3vFi7iYOADSmeubfExL6YMCxGsAOI5wXodT2e+y6QQOTeX8w2mlh+oZsR27IuNgHWYJe5+Xkw9k7Qa9x46kiw38XyzISQGPOJzTeNTV8Nms5yuT5haVhRQ2pKLm7/KndYWkU1JlUyGp0/lDkvARutbwDtlOVSbrVAPsNEVkI3RDeeOxC4RHamqykCSaRDDLWFGdI3dXj0xGc9nTOAew/sNc13BCYWw1O18FZRsb8H5iZ3DLKvxsrf0beYHRZVmjM54nustiTW+f52uE+MypjA+aGNysebgp2ypLg1xjIuhgNWNW9NG0CLioa6OtPjbuSvOElB+kCUl6+qQYK3JQX4qmkNuQGnrpMf5v7ik2n2gShsuTLuZTgHFW0sWsafakUAJ+wM3VmNvTmiwb7Kx26snSguGJPUN2HnUNvl+KNm+KCjZfimjwqUd1G3MKdkRVJyS8HduqcU2oui6cX36eFoGPuBbwCfO4jof/NCimNnsEhNuZJihthOFNAYkj6ZZQKmz4hs2hp1WRc/v/R+zgvIZA7gl8SQCQ2ckjMaRXGcNeKV3OT96qxtj+Ql6a2oZ7gbbehu7vfqiIzluPIMDtnCH47/85It0eFLEmqBku56eiUNpGWbH6nQvYHnQ6OoUrk2O5IBKVRbzOG6OCwyOLtaW1uWop3bFSMBK5qyEaKYYYDadxzmBqQD1d35wu+qjYwH8HHQuZnNAqc16Gb2iPnJjo3tC94CRwG5+cpUQ+meVSIvAptwL+M7rCfnsumvs9uqPouvG2ITmAaXrmVWeW/2Uu5LQyfYBjLOmhfnheNhfsTYod6WzXMagWu+QAVI5PbF/0PTT4/qO7cfhtDA2ApbSmYGmaMYngNKK/hZbQGEJW932BkhMVpDtyAoYihs4Jf7EOpwmpGA1nhaUIN7nysEd8ukZdDcHhvYdvJP3HPPcZdUEuTpo7PbqlZkOtkvpGFAaWfLdw97yWUE7p5SEsWGT7VDGjor9QaUnxvcgueYOhxRvHkaXwCb9WWz0xf5Oo77FRsAydK/DEn8zmeagWT757nzq/etUC9jicQYUtqCbMY66HGU26FsFbcxFHns1/bfS2TY4eOP3LuPl/aMZn/s808rWkO3z1FOOo7Hbq18G4zAuDzyw4f2GOTUl39UdfFUSmGzP5ALbKeHPSFDz2Bo0fEujqyk56h+bonSgV9ABxTw2e0Lu0o5pMbEOS9G3IinqX7yOeEMGZrKoPAks8xbhhqjyStVSC9kZNGzbx/z8O/lZqUPIUovZFljkt+MAAseOAFbz9dxmXc4zQWujVA44FzLNuZBpgMVwMj0s3elu7k4PSw+6GFOiWlTb2O3VrzTOSDqLeOdyjq70O5R8n2g5I+go81+qS7aPNNbwjtRi9gYNO9PpqK/DJ6Gk0t5ggCrr9rzYvaX4aIiT6GNXTAQssy6+Th3R6ZKJgyoBS1XLQ0+p6kBViygIsUo9z72FWp5QFEFjTrwqhBy6Kc0Ylv4iuQfu4QNn4IjvKKd3K6vLtrK6bNahAl0belmHMjxhJEMsLSIP5o3dXr1SsFnGcZ5+OV9UCkAVjv/yk68f54XMPR5kTcmKgGS7gV6JQ2o8iq2qJZQGDTUTSavTCe56bIYECFwq7Xfgh+MqYMXElFCn6Oo0pQJ98BtRvfWeX1GpqP9pZrX8YadYOl0Xrmn+H15MOZvWEVe5h3VlH/HP3PGM3vcoHztyiPTQRGO3V6+UE7k4sV1A4Xpmlode7xYy2a4byDhrao0/GFV1Bm8jSlwdR5oKRiV4l+7xe2NyGt6QYiJgefyeugWXkBuJpUGmIzG1gSjp9El6ko/bTueFtMsZbG4e8Xv2eFby7wNXclPhMnIjfVON3V69MdI6fizdA0q3lywIkXz3sLd8dlCyPTV+DD0juVqCEmLnia/OO89Quy9dXdIQGhUTU0KP344TolrWcOj1+ZXyE4cYdUnVnBoTPYW44DqVQbzU5gl6N2Ho1+nacLptEqfbJuFXC9jp2sAG5wZ+c65nvfNPAtfkH6Wyp/RJ/oaN99L6VpvPaer26oPOcBbjrW+w0VEp2e79hjmuq7iv8ikw6g6+KtkV8OpWjLadHNGUVqeE2EbU8hAn7NeGH6cveCpuUkx1nJlESo2ZHXVMBCy828hXR0ZwSk4oHgrdB4L2YMnGlHp/c4qSQoYOqjSm5pGr+omRwSo6JZ2OlqF0tAzlEgDKyXGt4YfypSwq+z7k2p280heYljCdO821zzI1dnvRS6RX0nBSHPMqrZE6yNKSX5loOfNI8Ax9GZkxjKgp2X5EMumB2whF5Pi8EPLcwUg4KfQG7pLBZrBVk7+q3zCm4qr3fHC0YuNX5tvIFl+0q6bK2OHcF1CmkGnOqPcRFko6HYO2kH1siOnDy/G0NA9mbOpTvN9mNq+mDiAl6Dn5LCj+jTJNthe5ONMlXBQQdyocMyqtfD/I2pKVASvIjfRJGkTzSGOAkkr7oG0kjy3eOmTv1Dy2BCXbEmhliKv2B6xXAjvhwalGNzH1+XJiZlV9bAQstrPUeTCqeb7q28hiV+CAtS39TAn1P1xWmtHVHLjQoJT1FbtjZg8UlpLGqYmP82rqKUGfTYVrVZjzFzXSXo39acuIxM4BhUeT737vD8yoCAgsurMYF1eLk7WVdE42Bl77toI/nPui3kZ8vo38GrQ/z6S7obpr7Oow6QJHrx6Kfe6ofmMVno3kRPG6hhAjAUtlU/Fi9tZ6A/aR5/ic1YGvMwykn6EhZrtm2lt7BOUy8srmkxUrk/waGWkTfzmnBRb7djXQhfYau71w9GRYx9M/IHpuL5nPLvXQyvbfA16RnjCGHrW6NHEcHazBFz7KK18RZYD2kedYRHZgseF0ulZ7OpieBENS0E7igDsviqPc5WxzbIqZHXKMBCzAM51/OQpqtQfw+9bw76LNQQnBtgnDadsg2UgFm+UCTg/81Hzf8EF5nmauUaTomtM6aNrijej8Oi20F45O35fx8UkBXZnPF85NzAtKtrdhtK22J7XrSLEMC7oCB96vmeEsq33yWt3GlyXbg4rTrQPDnh1iMXQk8GprBc4NFNSyA37fKj4vj+ri3w0idgIWFfxY8CxzI80HqbksKXw6OEGq9OEaW8Pc4ABA0Z3GlUEn1Hr4tfBFFmrghGAA1GLyA/cMSmrIVfWabC8sK10SRwVcsuUg8wue48vAbck8hhGG2m9JesNALrEERuhiFtlnsK1WAcPDntJXmRkU2VtxfkLHsDddMRlOC75LkPsrltRqGy1h7cE3g2cwTSiGAhagruO1nL/zibM47GjF789iTv6tPOcITtu2T7qNQQ16zWsLnZJuZWBgE+oapuROZUWdgpYfh+d/zCnLrnaBpc/3E58WryDbH+3KHh95jpmsDdwIjb1oFeJja+z2GoPJeAGXBRyRUX0HAqZLJvomnk2zqEbq6ZydPCL4YIPnYx6zrwmz7KMyP2WuD3nUviVo1mGKu54LTTUEUt3JDDUHfsB7+KxwcYRX5HWys+QJHiuz1/zURhQTActsPofOf20Y/l94L/cyrj/wNnMdm9nrc+DBj1ctYr/7Z74peoKb90zkVUfwB6mYJvBwUvv6PzoYQKc/i7vSzgq+tpF3Po/tu5PXynbU6giYqhaw1TGL1/Ku4oJ9D/Fq2b5qcw1+fzYLDz7GjbvHc0/hZyxz5VH9yTKB3OQ43uDBgnUB9St0sfULmkI0RXuNQmnB0KQ+4UfhurO5PC7aE5YV4i03cLs1+DTpnNL7ub1gCXvC3jnITa7jLe7K+ZSdQY914obUwRF8din0tZ0eNJ11Oadwb+FS9oZp3+/PZnHhbUy0/1LH9WP1LybWYekN5/GPRJU785ceXiPjZY/jv7zi+G8tKhnKIxlX1vFmmZHSkR7/IM+69/K34p1V8xLqFr4omMCcg905J2EwZ1i6cZKpJc10CZjwUuEvpcxfRL43m22urWxxrWd1xfYI97qVHTh837p3QNeBvtZedDN3prOpA630ySTqE4hXdHj9JRR4d7PdtZbvy75miSvoKuNguJRb4zNqOCetsdtrSDpS4sYzWLeWJdUMHJsljKF7ne4DmM6QtMn84Hos6GJ+e8ue4RrHHEYkXsRwa09OMqaTgA+Hbx87XD+ztHQW85wFIeo0cWrqI4yJaE2YjhTrjVxqWMWnAVPK/WVPcXXFbIYnnEN/Swda6Czo1HIKvTvZ4vyBpWXrq1xRt0Pi9VhLprGplp9AQ4iJgAUG0uMf4FW/h78Vrgy+cH+NLx/Joy3uYWhUF0iLVgI9UqbyIg/wYHFWUAJZ9W1kSfFGljT8TWLAn82asmzWRLOwSenCTRk307U2P87Gbq8BKLrujEnIYEnJgRCPnsBFtvA5okjo9GfzQPNbKMx5hw2BAxr/ZhYWbWZhLfZU7RKf48nENpHnZ5WTuCp9HCtyZxC4UhHfZpYUb65x+0y0/oNnkxJ4MUYWYsXElPAQM21tjzOt5W0MMUYaeBLplvgQH7V6gKFRJEfrLpneKa/xYbOLOLEef39GxdA4p1zoB3JnyylcabIcm+2FZaaj7RLah3hEMY9heMQr28NRiDON54XM+xlap6FBIn1T3uT11N4k1fzkKu3HW25mSvpgUqNoNSN+Mm82O5cWMXTOYqOPsHS69vSN603ekT2OjmRz0uHpgY4k81geazWKGysWs6D8f6x1biXbW3J0BKMk09rUlV5xgznPNogu+ijPp1LS6G7tjb7SlMBsaomp1pVZaBN/N+/GjWRh0Qd8XLIm6AYVkUmio3U45yeMYnhc+2rPqzQYhnJHmpNljv/xY0U2UQ3glDYMSLyJW5LOpm0NI53Gbq8xGQxnMMT4DjurJNjM9Es8K+g+AdFTsBhH8UirHgwqep23ilfX6lJEyeaLmZh2EyNMNd1OrTpGMhMe4QPDybxU8C4/eCPIuCsncX7qA9xmO+nQdhhDRwkbPWAZDefzQPPza3iWlTZxFzMx7mImHi5R8eLHUH95D6UzEzJeqq/a0OlO4fzUKYxMyWOzYyXfV/zCBtdWsj0HQySpDdgMJ9DO2J4O5i70tPSlt7kNiZFcDEBpSV/bBPraJoBaxn73Jja6NvK7awd/evay15uL3e8NeE0KLY1t6WjuQa+4gZwV14n0CPeajd1eY/J6V7Mi8OiGbjDjok62V09R2jAo5QXOStrDL+VLWO5Yw2/ubewLuv18As1NnekR15/B8cPoZwp9f87a0ZNiuYKnW11AdsUiFpX/yC+u7ez2lhxeEKpg0bemg6kHp8efw4j4nlVHVcopTGj+NBdXClw6XQfSm+ArjZEcVs2U+gxWDUhRmtM1fixd48ceLlHxqhVU+F34FDNWJQ5Tff14lQQyzf3JNPfnvKAHfXhUBUOdrzXWhO01KA+7yuYSeDOujITL6NqAiRKdrg19bTfS13YjACpuKvwVuFQFk86KtSHTAYqNDtYxTLKOqeXrUugcN7Bh+lRLmglY2qVgUKzY9NHfpiI6eoyNGjkau7068m9mdlngzSLac7GtQ52T7bWhYMKqM9XhJibHlxhKugvRWPwUVnzC0oDlBgbLeM41yj48lknAEscfNYuZB1cHnE2QwKCkAaQ1UZdEZCRgieNMOZuLnufzwFy3cSxXWxrgkkSiXsn4VxyjVPyq5/ByGBW/Wka+ZwMrS6bx7/LAqzIkMCTlEk6QaBXzJGCJY5O6i4/3Xc+0CK5hY4m7nf+LS5TphgbIdySOb/ohPJA+PPJLIIsmJSMscdxSjOfxYMY9DNVrYYWfAAlY4jhj0DWnvbknZ8SP5qL4blHeqUk0FQlY4tiktOP61t9xfRN3Q9QvyWEJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0Q7UireMAAAF3SURBVAKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0QwKWEEIzJGAJITRDApYQQjMkYAkhNEMClhBCMyRgCSE0Q+f2uJu6D0IIUSO3x41uw7ZNTd0PIYSo0eqNa9HNWDirqfshhBA1mvfd1+jenfUBWbu2NXVfhBCiWhuyNvHurA/QOd0uLrxjDBuyZGoohIg9G7I2ceGdl+F0u9ADjxeVFjP9q0/Yn59Leko6zVLS0ev1Td1PIcRxqtRRxrZd23n4jce5/dm7KXOUA/D/7klgiELbGTYAAAAASUVORK5CYII="

describe 'Api', ->
  beforeEach ->
    @faucet_id = '55cf717fa152e56c041f32fd'
    @captcha_id = '55deeec4c339478f4d52fe1b'
    @api = new Api 'test1@dub.co.in', '721635467764'

  describe 'new', ->
    it 'should be initialized with email and key', ->
      expect(@api.email).toBe 'test1@dub.co.in'
      expect(@api.key).toBe '721635467764'

  describe '#profile', ->
    it 'should be available to receive profile', (done) ->
      @api.profile (profile_json) ->
        expect(typeof profile_json).toBe 'object'
        expect(profile_json.nickname).toBe 'Collector'
        done()

  describe '#faucets', ->
    beforeEach (done) ->
      _t = @
      @api.faucets (list) ->
        _t.faucets            = list
        _t.without_last_claim = list[1]
        _t.with_last_claim    = list[0]
        done()

    it 'should be available to receive faucets list', ->
      expect(typeof @faucets).toBe 'object'
      expect(@faucets.length).toBe 2
      expect(@without_last_claim.events.length).toBe 0

    it 'should set last claim time for faucet, that user claimed', ->
      expect(@with_last_claim.events[0].created).toBe '2015-08-16T17:07:05.988Z'

  describe '#faucet', ->
    it 'should be available to get any faucet data to test', (done) ->
      @api.faucet 'Faucet1', (faucet) ->
        expect(typeof faucet).toBe 'object'
        expect(faucet.name).toBe 'Faucet1'
        done()

  describe '#log', ->
    it 'should store claim event', (done) ->
      @api.log {type: 'claim', amount: 1000}, @faucet_id, (result) ->
        expect(result.success).toBe true
        done()

    it 'should store wrong captcha event', (done) ->
      @api.log {type: 'wrong_captcha', captcha_id: '123456'}, @faucet_id, (result) ->
        expect(result.success).toBe true
        done()

    it 'should store low faucet balance event', (done) ->
      @api.log {type: 'low_faucet_balance'}, @faucet_id, (result) ->
        expect(result.success).toBe true
        done()

    it 'should store unhandled error event', (done) ->
      @api.log {type: 'unhandled_error'}, @faucet_id, (result) ->
        expect(result.success).toBe true
        done()

  describe '#send_captcha', ->
    it 'should create a new captcha', (done) ->
      @api.send_captcha base64_png, (result) ->
        expect(result.success).toBe true
        expect(typeof result.captcha).toBe 'object'
        expect(result.captcha.base64_png).toBe base64_png
        done()

  describe '#get_captcha_solution', ->
    it 'should receive solution for captcha id', (done) ->
      _t = @
      @api.get_captcha_solution @captcha_id, (result) ->
        expect(result.success).toBe true
        expect(typeof result.solution).toBe 'string'
        expect(result.solution).toBe 'drankl'
        done()
