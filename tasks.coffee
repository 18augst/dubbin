logger = require('log4js').getLogger 'PROCESS'
App    = require './lib/app'

desc 'Start dubbin'
task 'default', async: true, ->
  logger.info 'App started (press CTRL+C to stop)'
  new App (app) ->
    logger.warn 'HAVE A NICE PROFIT $)'
    app.start -> logger.error 'YOU SHOULD NEVER SEE THIS'

desc 'Debug a faucet'
task 'test', async: true, (faucet) ->
  new App (app) ->
    app.test faucet, (err) ->
      if err then console.error 'Test failed.'
      else
        console.log 'Test completed successfully.'
        app.browser.close()
      complete()
